userInfo = {
    getOsInfo: function() {

        if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1)  {
            return "Windows 10";
        }
        if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) {
            return "Windows 8";
        }
        if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) {
            return "Windows 7";
        }
        if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) {
            return "Windows Vista";
        }
        if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) {
            return "Windows XP";
        }
        if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) {
            return "Windows 2000";
        }
        if (window.navigator.userAgent.indexOf("Mac") != -1) {
            return "Mac/iOS";
        }
        if (window.navigator.userAgent.indexOf("X11") != -1) {
            return "UNIX";
        }
        if (window.navigator.userAgent.indexOf("Linux") != -1) {
            return "Linux";
        }
    },
    getResolution: function() {
        return window.screen.availWidth +'x'+ window.screen.availHeight;
    },
    getLang: function() {
        return navigator.language || navigator.userLanguage;
    },
    getBrowser: function() {
        var browser = 'unknown';

        if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
            browser = 'opera';
        }
        else if (navigator.userAgent.indexOf("Chrome") != -1) {
            browser = 'chrome';
        }
        else if (navigator.userAgent.indexOf("Safari") != -1) {
            browser = 'safari';
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            browser = 'firefox';
        }
        else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
        {
            browser = 'ie';
        }
        return browser;
    },
    getFingerprint: function() {
        return localStorage.getItem('fingerprint');
    },
    setFingerprint: function() {
        var fingerprint = this.getUserIP().split('.').join('');
        localStorage.setItem('fingerprint', fingerprint);
    },

    // TODO: generate this data
    getUserIP: function() {
        return '213.125.134.122';
    }
};
