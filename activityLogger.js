activityLogger = {
    logClick: function(activityType, activityLabel, activityAction, activityObject) {

        var data = new FormData();

        //Set custom added fields
        data.append("activityType", activityType);
        data.append("activityLabel", activityLabel);
        data.append("activityAction", activityAction);
        data.append("activityObject", activityObject);

        //Set default user info
        data.append("os", userInfo.getOsInfo);
        data.append("screen_size", userInfo.getResolution);
        data.append("language", userInfo.getLang);
        data.append("browser", userInfo.getBrowser);
        data.append("domain", this.domain);

        // TODO: make this values available somehow
        data.append("ip", userInfo.getUserIP);
        data.append("fingerprint", userInfo.getFingerprint);

        if (! userInfo.getFingerprint()) {
            userInfo.setFingerprint();
        }

        this.sendLog(data)
    },

    /*
    logSomethingElse()
    logMore()
    LogEvenMore()
     */
    sendLog: function(data) {

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                console.log(this.responseText);
            }
        });

        xhr.open("POST", "http://ma.app/api/log");
        xhr.setRequestHeader("authorization", "Basic am9yaXNAYWxpZW5zLm5sOmFsaWVuc2FkbTFu");
        xhr.setRequestHeader("cache-control", "no-cache");

        xhr.send(data);
    }
}